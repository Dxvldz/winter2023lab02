import java.util.Scanner;
public class PartThree {
    public static void main(String[]args){
        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter the first value.");
        Double one = scan.nextDouble();

        System.out.println("Please enter the second value.");
        Double two = scan.nextDouble();

        System.out.println("Result of the 2 values added to each other : " + calculator.add(one,two));
        System.out.println("Result of the 2 values subtract to each other : " + calculator.subtract(one,two));

        calculator cal = new calculator();
        System.out.println("Result of the 2 values multiplied to each other : " + cal.multiply(one,two));
        System.out.println("Result of the 2 values divided to each other : " + cal.divide(one,two));


    }
}
