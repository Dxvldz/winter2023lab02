public class MethodsTest {
    public static void main(String[]args){
        int x = 5;
        String s1 = "java";
        String s2 = "programming";
        System.out.println(x);

        methodNoInputNoReturn();
        System.out.println("This is before calling the One Input method");
        
        methodOneInputNoReturn(x+10);
        System.out.println("Final value : " + x);
        
        methodTwoInputNoReturn(1,2.4);
        int z = methodNoInputRetunrInt();
        System.out.println("Value of z  : " + z);
        
        double square = sumSquareRoot(9,5);
        System.out.println("Value of SumSquare  : " + square);

        System.out.println("This is the length for the first string : " + s1.length());
        System.out.println("This is the length for the second string : " + s2.length());

        System.out.println(SecondClass.addOne(50));
        SecondClass sc = new SecondClass();
        System.out.println(sc.addTwo(50));
    }
    public static void methodNoInputNoReturn(){
        int x = 20;
        System.out.println("I'm in a method that takes no ipnput and returns nothing");
        System.out.println(x);
    }
    public static void methodOneInputNoReturn(int y){
        System.out.println("Inside the method one input no return");
        System.out.println("The value of the input is subtracted by 5");
        System.out.println(y);
    }
    public static void methodTwoInputNoReturn(int one, double two){
        System.out.println("Two input: int - " + one + " double - " +two );
    }
    public static int methodNoInputRetunrInt() {
        return 5;
    }
    public static double sumSquareRoot(int one, int two) {
        int three = one+two;
        return Math.sqrt(three);
    }
}
